#!/bin/sh

echo "ok"
echo "${key123}"
echo "${keyabc}"

USER_UID=1000
WORKDIR=$PWD
GIT_NAME="aurauto"
GIT_EMAIL="aurauto@fos.bot"

cat /etc/issue
uname -a
# set -e
set -x

cat /etc/pacman.conf
cat pacman_extra_servers.conf >> /etc/pacman.conf
pacman-key --init && \
    pacman -Sy --noconfirm archlinux-keyring && \
    pacman -Syu --noconfirm libarchive attr wget git yasm yajl && \
    pacman-db-upgrade && \
    update-ca-trust && \
    pacman -Scc --noconfirm

# base-devel cmake libconfig boost

cp sudoers /etc/sudoers
chown -c root:root /etc/sudoers
chmod -c 0440 /etc/sudoers
# sed -i 's/bsdtar/bsdtar --no-xattrs/g' /usr/bin/repo-add
cp -a repo-add /usr/bin/

useradd -u ${USER_UID} -d /home/makepkg -m makepkg
chown -R makepkg ${WORKDIR} /home/makepkg

function upbintray() {
    pkgfile=$1
    pkgname=$2

    UPURLPFX="https://api.bintray.com/content/kitech/os/aurcare/x86_64"

    set +x
    echo "curl -T $pkgfile -uxxx $UPURLPFX/$pkgname?publish=1&override=1"
    if [ -f "$pkgfile" ]; then
        curl -T "$pkgfile" -u${BINTRAY_KEY} "$UPURLPFX/$pkgname?publish=1&override=1"
        # curl -X POST -u${BINTRAY_KEY} "$UPURLPFX/publish"
    fi
    set -x
}

dirs=$(git show  --dirstat --oneline|cat|grep "^ " | awk '{print $2}')
if [ x"$dirs" = x"" ]; then
   exit
fi

for d in $dirs; do
    echo "changed... $d"

    #cd "$d"
    su -l makepkg -c "git config --global user.name $GIT_NAME"
    su -l makepkg -c "git config --global user.email $GIT_EMAIL"
    # su -l makepkg -c "cd $WORKDIR && makepkg --noconfirm -si && mksrcinfo"
    #su -l makepkg -c "cd $WORKDIR/$d && makepkg --clean --rmdeps --syncdeps --noarchive --noconfirm --noprogressbar --asdeps"
    su -l makepkg -c "cd $WORKDIR/$d && makepkg --clean --syncdeps --noconfirm --noprogressbar --asdeps --skippgpcheck"
    # su -l makepkg -c "cd $WORKDIR/$d && mksrcinfo"
    cd ${WORKDIR}

    ls ${d}
    pkgfiles=$(ls ${d}*pkg.tar.xz)
    if [ x"$pkgfiles" = x"" ]; then
        continue
    fi

    for pkgfile in $pkgfiles; do
        pkgname=$(basename ${pkgfile})
        pacman -U --noconfirm "$pkgfile"
        # curl 'https://1.bitsend.jp/jqu/' -F "files[]=@$pkgfile" | json_reformat
        curl --upload-file "$pkgfile" "https://transfer.sh/$pkgname"

        upbintray "$pkgfile" "$pkgname"

        if [ ! -f "aurcare.db.tar.gz" ]; then
            wget https://dl.bintray.com/kitech/os/aurcare.db.tar.gz
            wget https://dl.bintray.com/kitech/os/aurcare.files.tar.gz
        fi
        pwd ;

        repo-add aurcare.db.tar.gz "$pkgfile"
        rm -f aurcare.db aurcare.files
        cp aurcare.db.tar.gz aurcare.db
        cp aurcare.files.tar.gz aurcare.files
        upbintray "aurcare.db" "aurcare.db"
        upbintray "aurcare.db.tar.gz" "aurcare.db.tar.gz"
        upbintray "aurcare.db.tar.gz.old" "aurcare.db.tar.gz.old"
        upbintray "aurcare.files" "aurcare.files"
        upbintray "aurcare.files.tar.gz" "aurcare.files.tar.gz"
        upbintray "aurcare.files.tar.gz.old" "aurcare.files.tar.gz.old"
        echo ""
    done
done


# docker坑，https://lists.archlinux.org/pipermail/pacman-dev/2017-October/022162.html

